package keyboard

import (
	"bufio"
	"os"
	"strconv"
	"strings"
)

// эта функция читает данные с клавиатуры (числа)
func GetNumber() (float64, error) {
	reader := bufio.NewReader(os.Stdin)
	input, err := reader.ReadString('\n')
	if err != nil {
		return 0, err
	}
	input = strings.TrimSpace(input)
	//input = strings.Map(func(r rune) rune {
	//	if unicode.IsPrint(r) {
	//		return r                           удаляет все символы не являющиеся буквами в строке
	//	}
	//	return -1
	//}, input)

	number, err := strconv.ParseFloat(input, 64)
	if err != nil {
		return 0, err
	}
	return number, nil
}
