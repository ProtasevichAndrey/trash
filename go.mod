module test_paths

go 1.20

require (
    test_paths/greeting  v1.0.0
    test_paths/keyboard v1.0.0
)

replace test_paths/greeting => ./greeting
replace test_paths/keyboard => ./keyboard